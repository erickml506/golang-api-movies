package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
	"log"
	"os"
)

type User struct {
	Id        int    `json:"id"`
	FirstName string `json:"firstName"`
	LastName  string `json:"lastName"`
	Age       int    `json:"age"`
}

func LoadEnvs() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	s3Bucket := os.Getenv("S3_BUCKET")
	secretKey := os.Getenv("SECRET_KEY")
	fmt.Println(s3Bucket, secretKey)
}

func main() {

	data := make(map[string]User)

	data["1"] = User{1, "erick", "mendoza", 25}
	data["2"] = User{2, "erick1", "mendoza1", 21}
	data["3"] = User{3, "erick2", "mendoza2", 22}
	data["4"] = User{4, "erick3", "mendoza3", 24}

	r := gin.Default()
	r.GET("/", func(c *gin.Context) {
		c.JSON(200, data)
	})

	err := r.Run(":1718")

	if err != nil {
		log.Fatal("No se pudo cargar el server")
	}

}
