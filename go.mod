module gitlab.com/erickml506/golang-api-movies

go 1.14

require (
	github.com/gin-gonic/gin v1.5.0
	github.com/joho/godotenv v1.3.0
)
